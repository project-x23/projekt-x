//Settings
var startButton = document.getElementById("startButton");



function initializeRanking(){
	startButton.style.display = "none";
    document.write("Czesc z rankingiem");
 }
 
function initializeStart(){
    startButton.style.display = "none";


	
	
var map = {
	read(key){
		var state = this[key];
		if(state == "keyup") this[key] = 0;
		return state;
	},
	"w":0,
	"s":0,
	"a":0,
	"d":0,
	" ":0, 	// zmienic na enter
	"Pause":0,
};


onkeydown = onkeyup = function(ev){
	
	if(map[ev.key]!=undefined) map[ev.key] = ev.type;
	
};	


	class AnimatedImage{
		constructor(array, interval){
			this.length = array.length;
			this.array = [];
			for(var i=0; i < this.length; i++){
				this.array[i] = new Image();
				this.array[i].src = array[i]; 
			}
			
			this.currentImage = 0;
			var that = this;
			setInterval(function(){
				that.nextImage();
			}, interval);
		}
		nextImage(){
			this.currentImage++;
			if(this.currentImage >= this.length)
				this.currentImage = 0;
		}
		getImage(){
			return this.array[this.currentImage];
		}
	}

	
	var explosionImage = 
	new AnimatedImage(
		[
			'images/ex1.png', 
			'images/ex2.png', 
			'images/ex3.png', 
			'images/ex4.png', 
			'images/ex5.png', 
			'images/ex6.png',
			'images/ex7.png', 
			'images/ex8.png', 			
		],
		30
	);
		
	var bullet = new Image();
	bullet.src = 'images/bullet.png';

	var backgroundImage = new Image();
	backgroundImage.src = 'images/tlo.png';

	var playerImage = new Image();
	playerImage.src = 'images/spaceship.png';

	var enemyImage = new Image();
	enemyImage.src = 'images/asteroid.png';

	var healthImage = new Image();
	healthImage.src = 'images/yesLife.png';

	var noHealthImage = new Image();
	noHealthImage.src = 'images/noLife.png';


	
	
var canvas = document.querySelector("canvas");
canvas.width = innerWidth;
canvas.height = innerHeight;
var ctx = canvas.getContext("2d");  //ctx context
ctx.font = "100px arial";


////////////////// BULLET

class Bullet {
	constructor(image,movementSpeed,pX,pY,width,height,damage){
		
		this.image = image;
		this.pX = pX;
		this.pY = pY;
		this.width = width;
		this.height = height;
		this.movementSpeed = movementSpeed;
		this.damage = damage;
	}

	move(x,y) {
	
		this.pY -= this.movementSpeed;
		

	}
	draw(context){
		context.drawImage(this.image, this.pX, this.pY,this.width,this.height);
	}
}
/*
class testowyPocisk extends Bullet{
	constructor(){
		super(
			bullet,
			Player.pX,  /////////////////////////////////////////////////////
			Player.pY,
			20,
			40,
			10,
			5,
			
		);
	}
}*/

	var bulletArray = [];


	function createBullets(playerPX, playerPY){
		//console.log("Tworzę bullet") 
			var tmp = new Bullet(bullet,20,playerPX,playerPY,20,20,5);
			bulletArray.push(tmp) 	
			//console.log(tmp)
	}


	function drawBullets(){
		
		for(var i=0;i<bulletArray.length;i++){
			bulletArray[i].draw(ctx)
			//console.log("Rysuję bullet")
		}
	}


	function moveBullets(){
		for(var i=0;i<bulletArray.length;i++){
			bulletArray[i].move(0,-1);
		}
	}
	function deleteBullets(){
		var i;
		for(i=0;i<bulletArray.length;i++){ 
			var usun = false;
			for(var n=0;n<enemyTab.length;n++){
				if( checkCollision(bulletArray[i],enemyTab[n]) == true ){   
					  usun = true                                                             //// jak bullet uderzy w gracza
					enemyTab[n].die(n);
		
				}
			}
			
			if(bulletArray[i].pY < 0){ 
				usun = true
				
				//console.log('Usuwanie bullet')
			}
			
			if(usun) bulletArray.splice(i,1) ;
			
		}
	}


///////////////// PLAYER

var data = new Date();

class Player {

    constructor(playerImage, healthImage, noHealthImage,lives, movementSpeed,pX,pY,width,height){
		
		this.playerImage = playerImage;
		this.healthImage = healthImage;
		this.noHealthImage = noHealthImage;
		this.lives = lives;
        this.pX = pX;
        this.pY = pY;
		this.width = width;
		this.height = height;
		this.movementSpeed = movementSpeed;
		this.score = 0;
		this.bulletArray = null;

    }
	
	
	
	draw(context){
		 //Rysowanie statku gracza
		context.drawImage(this.playerImage, this.pX, this.pY,this.width,this.height);
		 
		 //Rysowanie zycia gracza
		 
		 if(this.lives>0){
			 context.drawImage(this.healthImage, 50, 50,30,30);
		 }
		 else{
			 context.drawImage(this.noHealthImage, 50, 50,30,30);
		 }
		
		 if(this.lives>1){
			context.drawImage(this.healthImage,100, 50,30,30);
		 }
		 else{
			 context.drawImage(this.noHealthImage,100, 50,30,30);
		 }
		 
		 if(this.lives>2){
			context.drawImage(this.healthImage, 150, 50,30,30);
		 }
		 else{
			 context.drawImage(this.noHealthImage, 150, 50,30,30);
		 }
		 
		 
		 var str = this.score+'';
		 var textLength =  str.length;
		 
		 drawText(this.score, "yellow", canvas.width - 95 - textLength*55, 150); // Rysowanie punktów

		 if(this.lives <= 0){
			
				
				context.fillRect(0, 0, canvas.width, canvas.height);
				context.font = "italic bold 100px Arial";
				context.fillStyle = '#96A6E1'
				context.fillText("Koniec gry", 100, 240);
					
			}
		 
	}
     
    move(x,y) {
		   this.pX += x* this.movementSpeed;
		   this.pY += y* this.movementSpeed;
		   
		   if(this.pX < 0) this.pX = 0;  // Zapobiegam wyjeżdżaniu postaci poza ekran
		   else  if(this.pX + this.width > canvas.width) this.pX = canvas.width - this.width;
		   
		   if(this.pY < 0) this.pY = 0;  // Zapobiegam wyjeżdżaniu postaci poza ekran
		   else  if(this.pY + this.height > canvas.height) this.pY = canvas.height  - this.height;
	}
	
	shoot(){
		
		
			createBullets(this.pX, this.pY);
		
		
	}
	
	hitByEnemy(){
		
			this. lives -= 1;
			
	}
       
}


//////////// ENEMY

class Enemy {

    constructor(enemyImage,movementSpeed,pX,pY,width,height, explosionImage, livesImage, lives){
		
		this.enemyImage = enemyImage;
		this.pX = pX;
		this.pY = pY;
		this.width = width;
		this.height = height;
		this.movementSpeed = movementSpeed;
		this.alive = true;
		this.explosionImage = explosionImage;
		this.livesImage = livesImage;
		this.lives = lives;                             ///// ilość żyć
		
    }
	
	draw(context){
		if(this.alive){
			var tmpHeight = -this.height/2
			//this.enemyImage, this.pX, this.pY,this.width,this.height
			context.fillStyle = "#ff19a7";
			context.fillRect(this.pX, this.pY-20, this.lives*30, 10);
		     ////////////// wypisywanie paska życia
			context.drawImage(this.enemyImage, this.pX, this.pY,this.width,this.height);
		}
		

		else
		context.drawImage(this.explosionImage.getImage(), this.pX, this.pY,this.width,this.height);
	}
	
	die(n){
		
		//////////////////////////////////////////////////////////////////          uderzenie bullet
	this.lives -= 1;


		if(this.lives <= 0){
			this.alive = false;
			this.explosionImage.currentImage = 0;
			
			player.score++;
			
			setTimeout(function(){
				enemyTab.splice(n,1);
				},	260);
		}        
		
}
	
     
    move(x,y) {
		   this.pX += x* this.movementSpeed;
		   this.pY += y* this.movementSpeed;
	}
       
}


	function createEnemies(){
		
		var i=0;
		setInterval( function(){
			i=nextIndex(i)
			var size = randomSize();
			//                 enemyImage,movementSpeed,pX,pY,width,height
			var speed;
			var live;
			var liveImage;
			//max = 110
			//min = 50
			if(size < 70)	{ speed = 10;	live = 1; }
			else if(size < 90)	 { speed = 7; live = 2;  }
			else if(size < 110)	 { speed = 4; live = 3; }
			var tmp = new Enemy(enemyImage, speed,randomLocation(),0,size,size, explosionImage, liveImage, live);       /// ustawiam życie meteorytu
			enemyTab.push(tmp)
		}
		,700 );

	}
	createEnemies();


	function drawEnemies(){ 
		
		for(var i=0;i<enemyTab.length;i++){
			enemyTab[i].draw(ctx)

			//this.enemyImage, this.pX, this.pY,this.width,this.height
			//enemyTab[i].livesImage.draw(ctx);
		}
	}

	function moveEnemies(){
		for(var i=0;i<enemyTab.length;i++){
			enemyTab[i].move(0,1);
		}
	}

	function deleteEnemies(){
		var i;
		for(i=0;i<enemyTab.length;i++){            // sprawdza czy nic nie zostalo dotkniete trzeba dodac
		
			if(enemyTab[i].pY > innerHeight){   // jesli przejdzie cala plansze
				enemyTab.splice(i,1)  // usuwanie od indeksu "i" i ile
			}
			else if( checkCollision(player,enemyTab[i]) == true ){   
				
				/*enemyTab[i].lives -= 1;

				if(enemyTab[i].lives == 2){
					enemyTab[i].livesImage = healthSecond;
				}
				if(enemyTab[i].lives == 1){
					enemyTab[i].livesImage = healthOne;           /// zmiana obrazków health przy kolizji
				}*/
				

				//if(enemyTab[i].lives <= 0)           
				enemyTab.splice(i,1)  
				player.hitByEnemy();  //// jak gracz wpadnie w meteoryt
			}

		}
	}

		
	// EXTRA LIFE
	class ExtraLife {

		constructor(image,movementSpeed,pX,pY,width,height){
			
			this.image = image;
			this.pX = pX;
			this.pY = pY;
			this.width = width;
			this.height = height;
			this.movementSpeed = movementSpeed;
		}
		
		draw(context){
			context.drawImage(this.image, this.pX, this.pY,this.width,this.height);
		}
		
		move(x,y) {
			this.pX += x* this.movementSpeed;
			this.pY += y* this.movementSpeed;
		}
		
	}

	var additionalLife;

	function createExtraLife(){
		setInterval( function(){
			additionalLife = new ExtraLife(healthImage, 5,randomLocation(),0,40,40);
		}, 2000)
	}
	createExtraLife();

	function drawExtraLife(){ 
			additionalLife.draw(ctx)
			console.log("dodaje extra life")
	}

	function moveExtraLife(){
			additionalLife.move(0,1);
			console.log("move extra life")
	}

	function deleteExtraLife(){
		
		if(additionalLife.pY > innerHeight){   // jesli przejdzie cala plansze
			delete additionalLife;  // usuwanie od indeksu "i" i ile
		}
		else	if( checkCollision(player,additionalLife) == true){   
			additionalLife =undefined;  
			player.lives += 1;
		}
		console.log("delete extra life")
	}
   


	

	function  randomLocation(){
		max = innerWidth-100
		min = 100
		return Math.floor(Math.random() * (max - min)) + min;
		//return 100;
	}	
	function  randomSize(){
		max = 110
		min = 50
		return Math.floor(Math.random() * (max - min)) + min;
	}	
	function nextIndex(i){
		if(i<enemyTab.length-1){   
			return i+=1;
		}
		else{
			return 0;
		}
	}
		// used also for bullet detection
		//var length = Math.sqrt(Math.pow(player.pX-enemy.pX,2) + Math.pow(player.pY-enemy.pY,2));
	function checkCollision(player, enemy){
		if(typeof(enemy) !='undefined' && enemy != null){
			var length = Math.sqrt(Math.pow(player.pX+ (player.width/2) - (enemy.pX + (enemy.width/2)) ,2) + 
						 Math.pow(player.pY+(player.height/2) - enemy.pY,2));  // obliczanie promienia

			if(length < enemy.width/2 + player.width/2 )
				return true;
		}
		return false;
	}

	



	var enemyTab = []
	//		playerImage, healthImage, noHealthImage,lives, movementSpeed,pX,pY,width, height
	var player = new Player(playerImage,  healthImage, noHealthImage,3,10,canvas.width/2-200,canvas.height-50-200,200,200);
	   







function drawText(text,color,posX,posY){

    var fillStyle = ctx.fillStyle; 
    ctx.fillStyle = color;           // background
    ctx.fillText(text, posX, posY);  // text
    
    
}

function step() {
 game.update();
 window.requestAnimationFrame(step);


 
} 
var tmp1 = 0;

var interval = null;
var shooted = false;

var game = {
 start(){

	step();
	
 },
 update(){
 
    ctx.fillStyle = "black";
	ctx.drawImage(backgroundImage, 0,0,window.innerWidth,window.innerHeight);
	
	
	
	player.draw(ctx);
	drawEnemies();
	moveEnemies();
	drawBullets();
	moveBullets();
	
	deleteEnemies();
	deleteBullets();
	
	if(typeof(additionalLife) !='undefined' && additionalLife != null){
		drawExtraLife();
		moveExtraLife();
		deleteExtraLife();
	}
	
	if(map.read("w") == "keydown")  player.move(0,-1);
	
	if(map.read("s") == "keydown")  player.move(0,1);
	
	if(map.read("a") == "keydown")  player.move(-1,0);
	
	if(map.read("d") == "keydown")  player.move(1,0);

	
		
	if(map.read(" ") == "keydown"){
		
		if(interval==null){
			
			interval = setInterval(function(){
				player.shoot()
				shooted = true;
			},100);
		}
		
	} else if(interval!=null){
		clearInterval(interval);
		interval = null;
		if(!shooted) player.shoot();
	} 
	// zmienić na enter
	
	
    
 }
}
game.start();


}
